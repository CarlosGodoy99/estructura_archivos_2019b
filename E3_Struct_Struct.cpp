#include <iostream>
#include <string>

using namespace std;

struct adress{
    char number;
    char state_number;
    char street;
    char colony;

};

struct persona{
    string name;
    int age;
    string gender;
    float weight;
    float height;
    string nationality;
    adress your_adress;

};

int main(){
    persona per;
    cout<<" --- Who Are You? --- "<<endl;
    cout<<"Name: "<<endl;
    cin >>per.name;
    cout<<"Age: "<<endl;
    cin >>per.age;
    cout<<"Gender: "<<endl;
    cin >>per.gender;
    cout<<"Weight: "<<endl;
    cin >>per.weight;
    cout<<"Height: "<<endl;
    cin >>per.height;
    cout<<"Nationality: "<<endl;
    cin >>per.nationality;

    cout<< "\nHello " << per.name <<endl;
    cout<< "your age is " << per.age <<endl;
    cout<< "your gender is " << per.gender <<endl;
    cout<< "your weight is " << per.weight <<endl;
    cout<< "your height is " << per.height <<endl;
    cout<< "and your nationality is " << per.nationality <<endl;

    per.your_adress.number = '1';
    per.your_adress.street = 'Z';
    per.your_adress.colony = 'B';
    per.your_adress.state_number = '5';

    //per.your_adress = my_adress;
    //Impresion

    cout<< "Your adress is: " << per.your_adress.number << ", colony: " << per.your_adress.colony << ", your street is: " << per.your_adress.street << " and the state number is: " << per.your_adress.state_number << endl;
    return 0;
}
