#include <iostream>
#include <string>

using namespace std;

struct persona{
    string name;
    int age;
    string gender;
    float weight;
    float height;
    string nationality;


};

int main(){
    persona per;
    cout<<" --- Who Are You? --- "<<endl;
    cout<<"Name: "<<endl;
    cin >>per.name;
    cout<<"Age: "<<endl;
    cin >>per.age;
    cout<<"Gender: "<<endl;
    cin >>per.gender;
    cout<<"Weight: "<<endl;
    cin >>per.weight;
    cout<<"Height: "<<endl;
    cin >>per.height;
    cout<<"Nationality: "<<endl;
    cin >>per.nationality;

    cout<< "\nHello " << per.name <<endl;
    cout<< "your age is " << per.age <<endl;
    cout<< "your gender is " << per.gender <<endl;
    cout<< "your weight is " << per.weight <<endl;
    cout<< "your height is " << per.height <<endl;
    cout<< "and your nationality is " << per.nationality <<endl;

    return 0;
}
